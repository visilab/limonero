# SmartPower2

Parte software del dispositivo SmartPower2. Se puede configurar y ajustar como si de un Arduino se tratara.  

Esta guía de instalación y compilación sólo funciona en sistemas con arquitectura x86.

## Instalación  

Haciendo uso del fichero Makefile proporcionado, se debe de ejecutar el siguiente comando:

```
make install
```

Si ocurre algún error durante este proceso, visite la [pagina web oficial](http://docs.platformio.org/en/stable/installation.html) de Platformio.  

## Compilación

Haciendo uso del fichero Makefile proporcionado, se debe de ejecutar el siguiente comando:  

```
make build
```

## Subida a la placa SmartPower2  

Haciendo uso del fichero Makefile proporcionado, se debe de ejecutar el siguiente comando:  

```
make upload
```

## Ejecución  

La ejecución de la aplicación se realiza a través de "telnet". Para ello, bastará simplemente con al ejecución del script log.sh incluido en este proyecto. El resultado del mismo puede ser consultado en el fichero log.txt.

También se puede ejecutar directamente a través de telnet, utilizando la siguiente órden:  

```
telnet 192.168.4.1 > log.txt
```

Durante la ejecución se aprecian cinco columnas:

- El tiempo (en horas:minutos:segundos) desde que se ha iniciado el dispositivo SmartPower2  
- Voltios consumidos  
- Amperios consumidos  
- Watios consumidos  
- Watios*hora consumidos

## Información adicional  

- [Web del dispositivo SmartPower2](http://odroid.com/dokuwiki/doku.php?id=en:acc:smartpower2)  
